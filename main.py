import os
import re
import sys
from datetime import datetime

folder_path = input("Enter the path to the folder containing text files: ")

if not os.path.isdir(folder_path):
    print("Invalid folder path.")
    sys.exit()

total_time = 0

# Regular expression to match timestamp in format <2022-02-20T18:19:49.516Z>
timestamp_regex = r'<(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z)>'

# Loop over all files in the folder
for file_name in os.listdir(folder_path):
    file_path = os.path.join(folder_path, file_name)

    # Check if the file is a text file
    if not file_name.endswith('.log'):
        print(f'Skipping {file_name} (not a log file)')
        continue

    with open(file_path, 'r') as f:
        file_contents = f.read()

        # Find the first and last occurrences of the timestamp in the file
        timestamps = re.findall(timestamp_regex, file_contents)
        if not timestamps:
            continue
        first_timestamp = datetime.fromisoformat(timestamps[0][:-1])
        last_timestamp = datetime.fromisoformat(timestamps[-1][:-1])

        # Calculate the time difference between the timestamps and add it to the total
        time_diff = last_timestamp - first_timestamp
        total_time += time_diff.total_seconds()
total_hours = total_time / 3600
print(f'Total time between timestamps in all files: {total_hours:.2f} hours')

input("Press enter to exit...")
